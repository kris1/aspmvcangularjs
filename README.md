# ASP.NET MVC and AngularJS Demo #

This is a sample project that get the data from back-end SQL Server and display over the page. and provides many functionalities such as Filter, Shorting, Searching, Pagination.
 
*Hope screenshots will explain more details.

### All the data loaded from Database. ###
![Final-Demo-ScreenShot1.png](https://bitbucket.org/repo/ykqbRy/images/672559865-Final-Demo-ScreenShot1.png)

### Searching through Product Title like "Iphone" ###
![Final-Demo-ScreenShot2.png](https://bitbucket.org/repo/ykqbRy/images/160163060-Final-Demo-ScreenShot2.png)

### Filters and Export functionality ###
*Note default is 15 for paging.
![Final-Demo-ScreenShot3.png](https://bitbucket.org/repo/ykqbRy/images/1209127583-Final-Demo-ScreenShot3.png)

### Searching through Price between ###
![Final-Demo-ScreenShot4.png](https://bitbucket.org/repo/ykqbRy/images/3065038443-Final-Demo-ScreenShot4.png)

## Feel Free to play around ##
**SQL Database**

```
#!SQL

CREATE TABLE [dbo].[tblProducts](
	[ProductID] [int] NOT NULL,
	[ProductTitle] [nvarchar](256) NULL,
	[Type] [nvarchar](50) NULL,
	[Price] [numeric](18, 2) NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_tblProducts] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
```

*Insert your products.

Cheers
Krishan

